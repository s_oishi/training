# - Find PROJ4
# This module defines
#  PROJ4_LIBRARY
#  PROJ4_FOUND, if false, do not try to link to
#  PROJ4_INCLUDE_DIR, where to find PROJ4.h
#
# Note that the expected include convention is
#  #include "proj_api.h"

FIND_PATH(PROJ4_DIR version.txt
    $ENV{PROJ4_DIR}
    ${PROJ4_DIR}
    ${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/proj4
    $ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/proj4
    NO_DEFAULT_PATH
)

FIND_PATH(PROJ4_INCLUDE_DIR proj_api.h
  PATHS
  ${PROJ4_DIR}/${SYSTEM_ID}
  ${PROJ4_DIR}/4.8.0/${SYSTEM_ID}/include
  ${PROJ4_DIR}
  $ENV{PROJ4_DIR}
  /usr/local
  /usr
  /sw # Fink
  /opt/local # DarwinPorts
  /opt/csw # Blastwave
  /opt
  PATH_SUFFIXES include/proj4 include
)

FIND_LIBRARY(PROJ4_LIBRARY
  NAMES proj proj_i
  PATHS
  ${PROJ4_DIR}/${SYSTEM_ID}/lib
  ${PROJ4_DIR}/4.8.0/${SYSTEM_ID}
  ${PROJ4_DIR}
  $ENV{PROJ4_DIR}
  /usr/local
  /usr
  /sw
  /opt/local
  /opt/csw
  /opt
  PATH_SUFFIXES lib lib64
)


INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set PROJ4_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PROJ4  DEFAULT_MSG  PROJ4_LIBRARY PROJ4_INCLUDE_DIR)

MARK_AS_ADVANCED(PROJ4_INCLUDE_DIR PROJ4_LIBRARY)
