#include <iostream>

#include <libCompress/ZlibCompressor.hpp>

int main(int argc, char** argv)
{
    std::string input;
    std::string output;

    if (argc < 3)
    {
        std::cerr << "compressApp: Missing parameters" << std::endl;
        return -1;
    }

    input = argv[1];
    output = argv[2];

    if(!compressFile(input, output))
    {
        std::cerr << "compressApp: Error compressing file" << std::endl;
        return -1;
    }

    std::cout << "compressApp: Success! Created " << output << std::endl;

    return 0;
}

