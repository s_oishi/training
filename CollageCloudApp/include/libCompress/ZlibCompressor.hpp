#ifndef _LIBCOMPRESS_ZLIBCOMPRESSOR_HPP_
#define _LIBCOMPRESS_ZLIBCOMPRESSOR_HPP_ 1

#include <string>

#include <zlib.h>

#include <libCompress/Export.hpp>

const unsigned int CHUNK = 0x4000;
const unsigned int windowBits = 15;
const unsigned int GZIP_DECODING = 32;
const unsigned int GZIP_ENCODING = 16;

extern LIBCOMPRESS_EXPORT bool compressFile(const std::string& inputPath, const std::string& outputPath);


#endif


