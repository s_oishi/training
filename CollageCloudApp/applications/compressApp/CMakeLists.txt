set(APP_NAME compressApp)

set(SOURCES
    main.cpp
)

set(HEADERS

)

include_directories(
    ${CMAKE_SOURCE_DIR}/include
    ${ZLIB_INCLUDE_DIRS}
)

SET(LIBRARIES 
    libCompress
    ${ZLIB_LIBRARY}
)

SETUP_LAUNCHER(${APP_NAME} CollageCloudApp)


