# - Find JPEG_12BIT
# This module defines
#  JPEG_12BIT_LIBRARY
#  JPEG_12BIT_FOUND, if false, do not try to link to
#  JPEG_12BIT_INCLUDE_DIR, where to find JPEG_12BIT.h
#
# Note that the expected include convention is
#  #include "proj_api.h"

FIND_PATH(JPEG_12BIT_DIR version.txt
    $ENV{JPEG_12BIT_DIR}
    $ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/jpeg
    ${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/jpeg
    NO_DEFAULT_PATH
)

FIND_PATH(JPEG_12BIT_INCLUDE_DIR jpeglib.h
  PATHS
  ${JPEG_12BIT_DIR}/6b2/${SYSTEM_ID}
  ${JPEG_12BIT_DIR}
  $ENV{JPEG_12BIT_DIR}
  /usr/local
  /usr
  /sw # Fink
  /opt/local # DarwinPorts
  /opt/csw # Blastwave
  /opt
  PATH_SUFFIXES include
)

FIND_LIBRARY(JPEG_12BIT_LIBRARY
  NAMES jpeg12bit
  PATHS
  ${JPEG_12BIT_DIR}/6b2/${SYSTEM_ID}/lib
  ${JPEG_12BIT_DIR}
  $ENV{JPEG_12BIT_DIR}
  /usr/local
  /usr
  /sw
  /opt/local
  /opt/csw
  /opt
  PATH_SUFFIXES lib lib64
)


INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set JPEG_12BIT_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(JPEG_12BIT  DEFAULT_MSG  JPEG_12BIT_LIBRARY JPEG_12BIT_INCLUDE_DIR)

MARK_AS_ADVANCED(JPEG_12BIT_INCLUDE_DIR JPEG_12BIT_LIBRARY)
