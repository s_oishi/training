# - Find ZLIB
# This module defines
#  ZLIB_LIBRARY
#  ZLIB_FOUND, if false, do not try to link to
#  ZLIB_INCLUDE_DIR, where to find ZLIB.h
#
# Note that the expected include convention is
#  #include "proj_api.h"

FIND_PATH(ZLIB_DIR version.txt
    $ENV{ZLIB_DIR}
    $ENV{ZLIB_ROOT}
    ${ZLIB_DIR}
    ${ZLIB_ROOT}
    ${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/zlib
    $ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/zlib
    NO_DEFAULT_PATH
)

FIND_PATH(ZLIB_INCLUDE_DIRS zlib.h
  PATHS
  ${ZLIB_DIR}/${SYSTEM_ID}
  ${ZLIB_DIR}/1.2.8/${SYSTEM_ID}/include
  ${ZLIB_DIR}
  $ENV{ZLIB_DIR}
  /usr/local
  /usr
  /sw # Fink
  /opt/local # DarwinPorts
  /opt/csw # Blastwave
  /opt
  PATH_SUFFIXES include/proj4 include
)

set(ZLIB_NAMES z zlib zdll zlib1)
set(ZLIB_NAMES_DEBUG zlibd zlibd1)

FIND_LIBRARY(ZLIB_LIBRARY
  NAMES ${ZLIB_NAMES}
  PATHS
  ${ZLIB_DIR}/${SYSTEM_ID}/lib
  ${ZLIB_DIR}/1.2.8/${SYSTEM_ID}
  ${ZLIB_DIR}
  $ENV{ZLIB_DIR}
  /usr/local
  /usr
  /sw
  /opt/local
  /opt/csw
  /opt
  PATH_SUFFIXES lib lib64
)

FIND_LIBRARY(ZLIB_LIBRARY_DEBUG
  NAMES ${ZLIB_NAMES_DEBUG}
  PATHS
  ${ZLIB_DIR}/${SYSTEM_ID}/lib
  ${ZLIB_DIR}/1.2.8/${SYSTEM_ID}
  ${ZLIB_DIR}
  $ENV{ZLIB_DIR}
  /usr/local
  /usr
  /sw
  /opt/local
  /opt/csw
  /opt
  PATH_SUFFIXES lib lib64
)

INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set ZLIB_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ZLIB  DEFAULT_MSG  ZLIB_LIBRARY ZLIB_INCLUDE_DIRS)

MARK_AS_ADVANCED(ZLIB_INCLUDE_DIRS ZLIB_LIBRARY)
