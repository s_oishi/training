# This module defines

# VPB_LIBRARY
# VPB_FOUND, if false, do not try to link to VPB
# VPB_INCLUDE_DIRS, where to find the headers
# VPB_LIBS, to use in target_link_libraries()

# to use this module, set variables to point to the VPB install directory
# VPBDIR or VPB_DIR

FIND_PATH(VPB_INCLUDE_DIRS vpb/Version
        HINTS
        $ENV{VPB_DIR}
        $ENV{VPBDIR}
        ${VPB_DIR}
        ${VPBDIR}
        PATH_SUFFIXES include
        PATHS
                /sw # Fink
                /opt/local # DarwinPorts
                /opt/csw # Blastwave
                /opt
                /usr/freeware
                /usr/
                /usr/local
                C:/Program Files/VPB
)

FUNCTION(VPB_FIND_LIBRARY module library)
        FIND_LIBRARY(${module} ${library}
                HINTS
                $ENV{VPB_DIR}
                $ENV{VPBDIR}
                ${VPB_DIR}
                ${VPBDIR}
                PATH_SUFFIXES
                        lib
                        lib64
                PATHS
                        /sw # Fink
                        /opt/local # DarwinPorts
                        /opt/csw # Blastwave
                        /opt
                        /usr/freeware
                        /usr/
                        /usr/local
                        C:/Program Files/VPB
        )
ENDFUNCTION(VPB_FIND_LIBRARY module library)

VPB_FIND_LIBRARY(VPB_LIBRARY vpb)
VPB_FIND_LIBRARY(VPB_LIBRARY_DEBUG vpbd)

SET(VPB_FOUND "NO")
IF(VPB_LIBRARY AND VPB_INCLUDE_DIRS)
  SET(VPB_FOUND "YES")
ENDIF(VPB_LIBRARY AND VPB_INCLUDE_DIRS)

if(VPB_LIBRARY_DEBUG)
    set(VPB_LIBS
        optimized "${VPB_LIBRARY}"
        debug     "${VPB_LIBRARY_DEBUG}"
    )
else()
    message(STATUS "Attention! Debug version of VPB not found. Release version of VPB will *crash* in debug build.")
    set(VPB_LIBS
        "${VPB_LIBRARY}"
    )
endif()
message(STATUS "VPB_LIBS=${VPB_LIBS}")
