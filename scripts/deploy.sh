#!/bin/bash

clear
echo "#############################################"
echo "### Topcon CollageCloud Dev Image Builder ###"
echo "#############################################"


while getopts n:i:v:u:p:e: option
do
    case "${option}"
    in
        n) name=${OPTARG};;
        i) pid=${OPTARG};;
        v) version=${OPTARG};;
        u) user=${OPTARG};;
        p) password=${OPTARG};;
        e) environment=${OPTARG};;
    esac
done

source vars.sh
version=`date '+%Y%m%d'v`$version

if [ -z "$user" ] && [ -z "$password" ] && [ -z "$environment" ] 
  then
    echo ""
    echo "The next options are mandatory:"
    echo ""
    echo "-n The name of your image"
    echo "-i The identifier of the process (pid)"
    echo "-v The version, 20180912v0.1.0"
    echo "-u The development user with connection with Collage Cloud"
    echo "-p The development password with connection with Collage Cloud"
    echo "-e The environment where you will deploy your image: alpha, beta, qa, production"
    echo ""
    echo "Example: $1 -n my-image-name -i process_id -v 20181010v0.1.0 -u user -p password -e alpha"
    echo ""
    exit 1
fi

if [ -z "$name" ]
  then
    echo "You need to set a name(-n) to identify your image"
    exit 1
fi 

if [ -z "$version" ]
  then
    echo "You need to set a version(-v) to identify your image"
    exit 1
fi 

if [ -z "$pid" ]
  then
    echo "You need to set a pid(-pid) to identify your image"
    exit 1
fi 

if [ -z "$user" ] || [ -z "$password" ]
  then
    echo "You need to set a user(-u) and password(-p) with connection with Collage Cloud"
    exit 1
fi

if [ -z "$environment" ]
  then
    echo "You need to set the environment(-e) where you will deploy your image"
    exit 1
fi

ecr_region="eu-west-1"
server_url="http://localhost:9500/"
server_login_path="api/users/login"
server_process_path="api/processes/$pid/"
case $environment
in
    alpha)    
        ecr_region="eu-west-1"
        server_url="https://collage-alpha-api.pointcloudviz.com/";;
    beta) 
        ecr_region="eu-central-1"
        server_url="https://collage-beta-api.pointcloudviz.com/";;
esac
echo "Region:" $ecr_region
echo "Profile:" $environment

source docker-build.sh $name $version $environment $user

echo $image

# get authentication token
if [ server_url != "http://localhost:9500/" ]
then
    echo "Getting authorization from AWS ECR for docker"
    eval $(aws ecr get-login --no-include-email --region $ecr_region --profile $environment  | sed 's/-e none//g')
    docker push $ecrUrl:$image
fi

#login to backend
echo "Starting session into the system ..."
body='{ "username": "'$user'", "password": "'$password'" }'
response=$(curl -X POST -H "Content-Type: application/json" -d "$body" $server_url$server_login_path)
authorization_token=$(jq -r '.token' <<<"$response")

# # update the image and version in the server
echo "Updating the system: "
body='{ "image": "'$image'", "name": "'$name'", "version": "'$version'" }'
curl -X PATCH $server_url$server_process_path -H "Authorization: JWT $authorization_token" -H "Content-Type: application/json" -d "$body"
