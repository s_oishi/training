# - Find QUAZIP
# This module defines
#  QUAZIP_LIBRARY
#  QUAZIP_FOUND, if false, do not try to link to
#  QUAZIP_INCLUDE_DIR, where to find QUAZIP.h
#
# Note that the expected include convention is
#  #include "proj_api.h"


FIND_PATH(QUAZIP_DIR version.txt
    $ENV{QUAZIP_DIR}
    $ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/quazip
    ${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/quazip
    NO_DEFAULT_PATH
)

FIND_PATH(QUAZIP_INCLUDE_DIR quazip/quazip.h
  PATHS
  ${QUAZIP_DIR}/0.7.2/${SYSTEM_ID}
  ${QUAZIP_DIR}
  $ENV{QUAZIP_DIR}
  /usr/local
  /usr
  /sw # Fink
  /opt/local # DarwinPorts
  /opt/csw # Blastwave
  /opt
  PATH_SUFFIXES include
)

FIND_LIBRARY(QUAZIP_LIBRARY
  NAMES quazip
  PATHS
  ${QUAZIP_DIR}/0.7.2/${SYSTEM_ID}/lib
  ${QUAZIP_DIR}
  $ENV{QUAZIP_DIR}
  /usr/local
  /usr
  /sw
  /opt/local
  /opt/csw
  /opt
  PATH_SUFFIXES lib lib64
)


INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set QUAZIP_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(QUAZIP  DEFAULT_MSG  QUAZIP_LIBRARY QUAZIP_INCLUDE_DIR)

MARK_AS_ADVANCED(QUAZIP_INCLUDE_DIR QUAZIP_LIBRARY)
