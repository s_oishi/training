#!/bin/bash
if [ -z "$1" ]
  then
    echo "No name argument supplied"
    exit 1
fi

if [ -z "$2" ]
  then
    echo "No version argument supplied"
    exit 1
fi

if [ -z "$3" ]
  then
    echo "Select deployment system [ alpha, beta, qa, production ]"
    exit 1
fi

if [ -z "$4" ]
  then
    echo "Set your username"
    exit 1
fi

name=$1
version=$2
system=$3
user=$4
ecrUrl="110224232539.dkr.ecr.eu-west-1.amazonaws.com/mirage/collagecloud"

echo "...................................."
echo "BUILDING: $system VERSION: $version "
echo "...................................."

if [ "$system" == "alpha" ]
  then
      echo "Configuring ALPHA (mcloud-alpha)..."
elif [ "$system" == "beta" ]
  then
    echo "Configuring BETA (mcloud-beta)..."
fi

image=$name-$user-$version-$system
cd ..
echo $ecrUrl:$image
docker build . -t $ecrUrl:$image
cd scripts
