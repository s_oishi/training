# Author Hui Ju (hju@topcon.com)
#
# CGAL_FOUND - system has CGAL lib/dll
# CGAL_INCLUDE_DIR - CGAL include directory
# CGAL_LIBRARIES - CGAL libraries
# CGAL_DEP_FOUND - GMP and MPFR are mandantory for CGAL
# CGAL_DEP_LIBRARIES - GMP and MPFR librarires

FIND_PATH(CGAL_DIR "version.txt"
		  $ENV{CGAL_DIR}
		  ${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/cgal
		  $ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/cgal
		  NO_DEFAULT_PATH
)
MESSAGE(STATUS "CGAL_DIR: " ${CGAL_DIR})

FIND_PATH(CGAL_INCLUDE_DIR gmp.h
		  ${CGAL_DIR}/4.10/${SYSTEM_ID}/include
	      ${CGAL_DIR}/include
	      NO_DEFAULT_PATH
)
MESSAGE(STATUS "CGAL_INCLUDE_DIR: " ${CGAL_INCLUDE_DIR})

# Following part is extendable, if more CGAL modules are needed, e.g. CGAL_ImageIO and CGAL_Qt5.
SET(Release "vc140-mt-4.10-I-900")
SET(Debug "vc140-mt-gd-4.10-I-900")
SET(CGAL_LIB_NAMES
	"CGAL_Core"
	"CGAL"
)

FOREACH(LIB_NAME ${CGAL_LIB_NAMES})
    SET(LIB_RELEASE "${LIB_NAME}_RELEASE")
	FIND_LIBRARY(${LIB_RELEASE} "${LIB_NAME}-${Release}"
		         PATH_SUFFIXES lib
		         PATHS ${CGAL_DIR}/4.10/${SYSTEM_ID}/lib
		               ${CGAL_DIR}/lib
		         )
	SET(LIB_DEBUG "${LIB_NAME}_DEBUG")
	FIND_LIBRARY(${LIB_DEBUG} "${LIB_NAME}-${Debug}"
				 PATH_SUFFIXES lib
		         PATHS ${CGAL_DIR}/4.10/${SYSTEM_ID}/lib
		               ${CGAL_DIR}/lib
			     )
	
	IF(${LIB_DEBUG})
		SET(CGAL_LIBRARIES ${CGAL_LIBRARIES} optimized ${${LIB_RELEASE}} debug ${${LIB_DEBUG}})
	ELSE()
		MESSAGE(STATUS "Attention! Debug version of ${LIB_NAME} is not found.")
		SET(CGAL_LIBRARIES ${CGAL_LIBRARIES} ${${LIB_RELEASE}})
	ENDIF()
	#MARK_AS_ADVANCED(${LIB_NAME})
ENDFOREACH(LIB_NAME)

# GMP_LIB
FIND_LIBRARY(CGAL_GMP_LIB "libgmp-10.lib"
	         PATHS ${CGAL_DIR}/4.10/${SYSTEM_ID}/lib
		           ${CGAL_DIR}/lib
)

# MPFR_LIB
FIND_LIBRARY(CGAL_MPFR_LIB "libmpfr-4.lib"
	        PATHS ${CGAL_DIR}/4.10/${SYSTEM_ID}/lib
		          ${CGAL_DIR}/lib
)

IF(CGAL_GMP_LIB AND CGAL_MPFR_LIB)
	SET(CGAL_DEP_LIBRARIES ${CGAL_DEP_LIBRARIES} ${CGAL_MPFR_LIB} ${CGAL_GMP_LIB})
ENDIF()

# Complete list of libs 
IF(CGAL_DEP_LIBRARIES) 
	SET(CGAL_LIBRARIES ${CGAL_LIBRARIES} ${CGAL_DEP_LIBRARIES})
ENDIF()
MESSAGE(STATUS "CGAL_LIBRARIES:${CGAL_LIBRARIES}")

# CGAL found is set to true only CGAL dependences and CGAL are both found
IF(CGAL_INCLUDE_DIR AND CGAL_LIBRARIES AND CGAL_DEP_FOUND AND CGAL_DEP_LIBRARIES)
	SET(CGAL_FOUND TRUE)
ENDIF()

# handle the QUIETLY and REQUIRED arguments and set
# TPSGEOSDK_FOUND to TRUE as appropriate
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(CGAL DEFAULT_MSG CGAL_INCLUDE_DIR CGAL_LIBRARIES)
MARK_AS_ADVANCED(CGAL_LIBRARIES)
