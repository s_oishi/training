# - Find XercesC
# This module defines
#  XercesC_LIBRARY
#  XercesC_FOUND, if false, do not try to link to
#  XercesC_INCLUDE_DIR, where to find XercesC.h
#


FIND_PATH(XercesC_DIR version.txt
    $ENV{XERCESC_DIR}
    $ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/xerces
    ${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/xerces
    NO_DEFAULT_PATH
)

MARK_AS_ADVANCED(XercesC_DIR)

FIND_PATH(XercesC_INCLUDE_DIR xercesc/dom/DOM.hpp
  PATHS
  ${XercesC_DIR}/3.1/${SYSTEM_ID}
  ${XercesC_DIR}
  $ENV{XERCESC_DIR}
  /usr/local
  /usr
  /sw # Fink
  /opt/local # DarwinPorts
  /opt/csw # Blastwave
  /opt
  PATH_SUFFIXES include
)

FIND_LIBRARY(XercesC_LIBRARY
  NAMES xerces-c_3 xerces-c xerces-c_3_1
  PATHS
  ${XercesC_DIR}/3.1/${SYSTEM_ID}
  ${XercesC_DIR}
  $ENV{XERCESC_DIR}
  /usr/local
  /usr
  /sw
  /opt/local
  /opt/csw
  /opt
  PATH_SUFFIXES lib lib64
)


INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set XercesC_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(XercesC  DEFAULT_MSG  XercesC_LIBRARY XercesC_INCLUDE_DIR)

MARK_AS_ADVANCED(XercesC_INCLUDE_DIR XercesC_LIBRARY)
