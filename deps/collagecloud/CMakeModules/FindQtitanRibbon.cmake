# Author Jesus Zarzoso <jzarzoso@topcon.com>

FIND_PATH(QTITAN_RIBBON_DIR src/ribbon/QtitanRibbon.h
    $ENV{QTITAN_RIBBON_DIR}
    $ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/QtitanRibbon/4.5.1/${SYSTEM_ID}
    ${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/QtitanRibbon/4.5.1/${SYSTEM_ID}
    NO_DEFAULT_PATH
)

# handle the QUIETLY and REQUIRED arguments and set
# QTITANRIBBON_FOUND to TRUE as appropriate
INCLUDE( FindPackageHandleStandardArgs )
FIND_PACKAGE_HANDLE_STANDARD_ARGS( QTITANRIBBON DEFAULT_MSG QTITAN_RIBBON_DIR)
