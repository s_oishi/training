include(FindGit)

if (GIT_FOUND)
    message("Init submodules")
    execute_process(
        COMMAND "${GIT_EXECUTABLE} submodule init"
        COMMAND "${GIT_EXECUTABLE} submodule update"
        WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    )
else()
    message("-- Git not found. You need Git for the Git submodule mechanism to work.")
endif()
macro(add_submodule _project _path)
    if (GIT_FOUND)
        message("Checkout master and pulling master for ${_project}")
        execute_process(
            COMMAND "${GIT_EXECUTABLE} checkout master"
            COMMAND "${GIT_EXECUTABLE} pull origin master --rebase"
            WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/${_project}"
		)
	endif()
	add_subdirectory(${_path})
endmacro()