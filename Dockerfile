FROM ubuntu:16.04
MAINTAINER Rafael Gaitán (Topcon Corporation)

ENV INSTALL_EXTERNALS_DEPS_DIR /app/src/app-sdk/thirdparty/lib
ENV EXTERNALS_DEPS_DIR /usr/src/app/deps/collagecloud

RUN apt-get update && apt-get -y upgrade --fix-missing --no-install-recommends && apt-get install -y apt-utils && \
    apt-get install -y build-essential \
                          cmake \
                          clang \
                          lsb-release \
                          locales \
                          zlib1g-dev \
    && mkdir -p /usr/src/app \
    && mkdir -p $INSTALL_EXTERNALS_DEPS_DIR

#Set the locale
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

ADD . /usr/src/app

RUN cd /usr/src/app \
    && mkdir build \
    && cd build \
    && export CC=clang && export CXX=clang++ \
    && cmake ../CollageCloudApp -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/src/app-sdk \
    && make -j `nproc` install \
    && cd

WORKDIR /usr/src/app-sdk/bin

# copy thirdparty deps
RUN find $EXTERNALS_DEPS_DIR -name '*.so*' -exec cp '{}' $INSTALL_EXTERNALS_DEPS_DIR \;

ENV LD_LIBRARY_PATH /usr/src/app-sdk/lib:$INSTALL_EXTERNALS_DEPS_DIR

# remove original source code and purge everything to reduce image size
RUN rm -rf /usr/src/app \
    && apt-get remove -y --purge build-essential \
                      cmake \
                      clang \
                      lsb-release \
                      *-dev \
    && rm -rf /var/lib/apt/lists/*
