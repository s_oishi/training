# Author Hui Ju <hju@topcon.com>

FIND_PATH(TPSGEOSDK_DIR version.txt
	$ENV{TPSGEOSDK_DIR}
	${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/tpsGeoSDK
    $ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/tpsGeoSDK
	NO_DEFAULT_PATH
)

MESSAGE(STATUS "TPSGEOSDK_DIR:" ${TPSGEOSDK_DIR})

FIND_PATH(TPSGEOSDK_INCLUDE_DIR dummy.h
	HINTS ${TPSGEOSDK_DIR}/4.3/${SYSTEM_ID}/include 
	      ${TPSGEOSDK_DIR}/include
		  $ENV{TPSGEOSDK_INCLUDE_DIR}
	NO_DEFAULT_PATH
)

MESSAGE(STATUS "TPSGEOSDK_INCLUDE_DIR:" ${TPSGEOSDK_INCLUDE_DIR})

FUNCTION(FIND_TPSGEOSDK_LIBRARY module library)
	#MESSAGE("info : " ${module} " " ${library})
    FIND_LIBRARY(${module} ${library}
    PATH_SUFFIXES
        lib
        lib64
    PATHS
        ${TPSGEOSDK_DIR}/4.3/${SYSTEM_ID}
    )
    MARK_AS_ADVANCED(${module})
ENDFUNCTION(FIND_TPSGEOSDK_LIBRARY module library)

# The list of full dependent lib names created in May 2017 based on MAGENET 4.3
SET(TPSGEOSDK_LIB_NAMES
	3DEngine
	3DMCFormatParsers
	3dmclib
	12DAParser
	adjustlib
	cogoapi
	customdatums
	customdfhbf
	DWG_2007Parser
	expat_static
	ExportGenerators
	ExportParsers
	geoidparsers
	geomath
	geoobj
	geosdkWrapper
	geosys
	haru
	ieLibrary
	ImportParsers
	MJBFormat
	rtcmtogff
	rtcmtransformation
	shpparser
	tableprojections
	tpsImportExport
	tpsmath
	tpsregistry
	tpsutils
)

FOREACH(LIB_NAME ${TPSGEOSDK_LIB_NAMES})
    SET(MODULE_RELEASE "TPSGEOSDK_${LIB_NAME}_LIBRARY_RELEASE")
	FIND_TPSGEOSDK_LIBRARY(${MODULE_RELEASE} "${LIB_NAME}")
	SET(MODULE_DEBUG "TPSGEOSDK_${LIB_NAME}_LIBRARY_DEBUG")
	FIND_TPSGEOSDK_LIBRARY(${MODULE_DEBUG} "${LIB_NAME}D")
	
	IF(${${MODULE_DEBUG}})
		SET(TPSGEOSDK_LIBRARIES ${TPSGEOSDK_LIBRARIES}
			optimized ${${MODULE_RELEASE}}
			debug ${${MODULE_DEBUG}}
			)
	ELSE()
		MESSAGE(STATUS "Attention! Debug version of ${LIB_NAME} is not found.")
		SET(TPSGEOSDK_LIBRARIES ${TPSGEOSDK_LIBRARIES} 
			${${MODULE_RELEASE}}
			)
	ENDIF()
ENDFOREACH(LIB_NAME)

message(STATUS "TPSGEOSDK_LIBRARIES=${TPSGEOSDK_LIBRARIES}")

# handle the QUIETLY and REQUIRED arguments and set
# TPSGEOSDK_FOUND to TRUE as appropriate
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(TPSGEOSDK DEFAULT_MSG TPSGEOSDK_INCLUDE_DIR TPSGEOSDK_LIBRARIES)
MARK_AS_ADVANCED(TPSGEOSDK_LIBRARIES)




	