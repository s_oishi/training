# - Find JPEG_12BIT
# This module defines
#  JPEG_12BIT_LIBRARY
#  JPEG_12BIT_FOUND, if false, do not try to link to
#  JPEG_12BIT_INCLUDE_DIR, where to find JPEG_12BIT.h
#
# Note that the expected include convention is
#  #include "proj_api.h"

FIND_PATH(JPEGTURBO_DIR version.txt
    $ENV{JPEGTURBO_DIR}
    $ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/libjpeg-turbo
    ${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/libjpeg-turbo
    NO_DEFAULT_PATH
)

FIND_PATH(JPEGTURBO_INCLUDE_DIR turbojpeg.h
  PATHS
  ${JPEGTURBO_DIR}/6b2/${SYSTEM_ID}
  ${JPEGTURBO_DIR}
  $ENV{JPEGTURBO_DIR}
  /usr/local
  /usr
  /sw # Fink
  /opt/local # DarwinPorts
  /opt/csw # Blastwave
  /opt
  PATH_SUFFIXES include
)

FIND_LIBRARY(JPEGTURBO_LIBRARY
  NAMES turbojpeg
  PATHS
  ${JPEGTURBO_DIR}/6b2/${SYSTEM_ID}/lib
  ${JPEGTURBO_DIR}
  $ENV{JPEGTURBO_DIR}
  /usr/local
  /usr
  /sw
  /opt/local
  /opt/csw
  /opt
  PATH_SUFFIXES lib lib64
)

INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set JPEG_12BIT_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(JPEG_12BIT  DEFAULT_MSG  JPEG_12BIT_LIBRARY JPEG_12BIT_INCLUDE_DIR)

MARK_AS_ADVANCED(JPEGTURBO_INCLUDE_DIR JPEGTURBO_LIBRARY)
