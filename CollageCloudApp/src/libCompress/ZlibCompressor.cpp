#include <fstream>
#include <iostream>
#include <vector>


#include <libCompress/ZlibCompressor.hpp>

bool compressFile(const std::string& inputPath, const std::string& outputPath)
{
    std::ifstream inputFile(inputPath, std::ios::binary);
    if (inputFile.eof() || inputFile.fail())
        return false;

    std::vector<unsigned char> inputBuffer;
    inputFile.seekg(0, std::ios_base::end);
    std::streampos fileSize = inputFile.tellg();
    inputBuffer.resize(fileSize);

    inputFile.seekg(0, std::ios_base::beg);
    inputFile.read((char*)(&inputBuffer[0]), fileSize);


    std::vector<unsigned char> compressedBuffer;
    z_stream strm;
    unsigned char out[CHUNK];

    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;

    deflateInit2(&strm, Z_DEFAULT_COMPRESSION, Z_DEFLATED,
        windowBits | GZIP_ENCODING, 8,
        Z_DEFAULT_STRATEGY);

    strm.next_in = inputBuffer.data();
    strm.avail_in = static_cast<unsigned int>(inputBuffer.size());
    strm.next_out = out;

    do
    {
        int have;
        strm.avail_out = CHUNK;
        strm.next_out = out;
        deflate(&strm, Z_FINISH);
        have = CHUNK - strm.avail_out;
        compressedBuffer.insert(compressedBuffer.end(), out, out + have);
    }
    while (strm.avail_out == 0);

    deflateEnd(&strm);
    inputFile.close();

    std::ofstream outputFile;
    outputFile = std::ofstream(outputPath, std::ios::binary);

    if (outputFile.eof() || outputFile.fail())
        return false;

    outputFile.write(reinterpret_cast<const char*>(compressedBuffer.data()), compressedBuffer.size());
    outputFile.close();
    return true; // c++11 ensures that this is doing a move so no new allocation for the buffer
}
