# This module defines

# OSGEARTH_LIBRARY
# OSGEARTH_FEATURES_LIBRARY
# OSGEARTH_QT_LIBRARY
# OSGEARTH_SYMBOLOGY_LIBRARY
# OSGEARTH_UTIL_LIBRARY
# OSGEARTH_FOUND, if false, do not try to link to osgEarth
# OSGEARTH_INCLUDE_DIRS, where to find the headers
# OSGEARTH_LIBS, to use in target_link_libraries()

# to use this module, set variables to point to the osgEarth install directory
# OSGEARTHDIR or OSGEARTH_DIR

FIND_PATH(OSGEARTH_INCLUDE_DIRS osgEarth/Layer
	HINTS
		$ENV{OSGEARTH_DIR}
		$ENV{OSGEARTHDIR}
		${OSGEARTH_DIR}
		${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/osgearth
		$ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/osgearth
	PATH_SUFFIXES include
	PATHS
		/sw # Fink
		/opt/local # DarwinPorts
		/opt/csw # Blastwave
		/opt
		/usr/freeware
		/usr/
		/usr/local
		C:/Program Files/osgEarth
)

FUNCTION(OSGEARTH_FIND_LIBRARY module library)
	FIND_LIBRARY(${module} ${library}
		HINTS
			$ENV{OSGEARTH_DIR}
			$ENV{OSGEARTHDIR}
			${OSGEARTH_DIR}
			${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/osgearth
			$ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/osgearth
		PATH_SUFFIXES
			lib
			lib64
		PATHS
			/sw # Fink
			/opt/local # DarwinPorts
			/opt/csw # Blastwave
			/opt
			/usr/freeware
			/usr/
			/usr/local
			C:/Program Files/osgEarth
	)
ENDFUNCTION(OSGEARTH_FIND_LIBRARY module library)

OSGEARTH_FIND_LIBRARY(OSGEARTH_LIBRARY osgEarth)
OSGEARTH_FIND_LIBRARY(OSGEARTH_LIBRARY_DEBUG osgEarthd)

OSGEARTH_FIND_LIBRARY(OSGEARTH_FEATURES_LIBRARY osgEarthFeatures)
OSGEARTH_FIND_LIBRARY(OSGEARTH_FEATURES_LIBRARY_DEBUG osgEarthFeaturesd)

OSGEARTH_FIND_LIBRARY(OSGEARTH_QT_LIBRARY osgEarthQt)
OSGEARTH_FIND_LIBRARY(OSGEARTH_QT_LIBRARY_DEBUG osgEarthQtd)

OSGEARTH_FIND_LIBRARY(OSGEARTH_SYMBOLOGY_LIBRARY osgEarthSymbology)
OSGEARTH_FIND_LIBRARY(OSGEARTH_SYMBOLOGY_LIBRARY_DEBUG osgEarthSymbologyd)

OSGEARTH_FIND_LIBRARY(OSGEARTH_UTIL_LIBRARY osgEarthUtil)
OSGEARTH_FIND_LIBRARY(OSGEARTH_UTIL_LIBRARY_DEBUG osgEarthUtild)

SET(OSGEARTH_FOUND "NO")
IF(OSGEARTH_LIBRARY AND OSGEARTH_INCLUDE_DIRS)
  SET(OSGEARTH_FOUND "YES")
ENDIF(OSGEARTH_LIBRARY AND OSGEARTH_INCLUDE_DIRS)

if(OSGEARTH_LIBRARY_DEBUG)
    set(OSGEARTH_LIBS
        optimized "${OSGEARTH_LIBRARY}"
        debug     "${OSGEARTH_LIBRARY_DEBUG}"
        optimized "${OSGEARTH_FEATURES_LIBRARY}"
        debug     "${OSGEARTH_FEATURES_LIBRARY_DEBUG}"
        optimized "${OSGEARTH_QT_LIBRARY}"
        debug     "${OSGEARTH_QT_LIBRARY_DEBUG}"
        optimized "${OSGEARTH_SYMBOLOGY_LIBRARY}"
        debug     "${OSGEARTH_SYMBOLOGY_LIBRARY_DEBUG}"
        optimized "${OSGEARTH_UTIL_LIBRARY}"
        debug     "${OSGEARTH_UTIL_LIBRARY_DEBUG}"
    )
else()
    message(STATUS "Attention! Debug version of osgEarth not found. Release version of osgEarth will *crash* in debug build.")
    set(OSGEARTH_LIBS
        "${OSGEARTH_LIBRARY}"
        "${OSGEARTH_FEATURES_LIBRARY}"
        "${OSGEARTH_QT_LIBRARY}"
        "${OSGEARTH_SYMBOLOGY_LIBRARY}"
        "${OSGEARTH_UTIL_LIBRARY}"
    )
endif()
message(STATUS "OSGEARTH_LIBS=${OSGEARTH_LIBS}")
