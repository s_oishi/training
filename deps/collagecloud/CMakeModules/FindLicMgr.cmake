# Author Jesus Zarzoso <jesus.zarzoso@mirage-tech.com>

FIND_PATH(LICMGR_DIR version.txt
    $ENV{LICMGR_DIR}
    ${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/licmgr
    $ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/licmgr
    NO_DEFAULT_PATH
)

FIND_PATH(LICMGR_INCLUDE_DIR licmgr/licmgr_extern.h
    ${LICMGR_DIR}/4.0/${SYSTEM_ID}/include
    ${LICMGR_DIR}/include
    $ENV{LICMGR_DIR}/include
    $ENV{LICMGR_DIR}
    $ENV{LICMGR_DIR}/include
    $ENV{LICMGR_DIR}
    NO_DEFAULT_PATH
)

FIND_PATH(LICMGR_INCLUDE_DIR licmgr/licmgr_extern.h)

FUNCTION(FIND_LICMGR_LIBRARY module library)
    FIND_LIBRARY(${module} ${library}
    PATH_SUFFIXES
        lib
        lib64
    PATHS
        ${LICMGR_DIR}/4.0/${SYSTEM_ID}
        ${LICMGR_DIR}
    )
    MARK_AS_ADVANCED(${module})
ENDFUNCTION(FIND_LICMGR_LIBRARY module library)

FIND_LICMGR_LIBRARY(LICMGR_LIBRARY_RELEASE licmgr)

FIND_LICMGR_LIBRARY(LICMGR_LIBRARY_DEBUG licmgrd)

SET(LICMGR_LIBRARIES_RELEASE
    ${LICMGR_LIBRARY_RELEASE}
)
MARK_AS_ADVANCED(LICMGR_LIBRARIES_RELEASE)

SET(LICMGR_LIBRARIES_DEBUG
    ${LICMGR_LIBRARY_DEBUG}
)
MARK_AS_ADVANCED(LICMGR_LIBRARIES_DEBUG)

if(LICMGR_LIBRARY_DEBUG)
    set(LICMGR_LIBRARIES
        optimized "${LICMGR_LIBRARY_RELEASE}"
        debug     "${LICMGR_LIBRARY_DEBUG}"
    )
else()
    message(STATUS "Attention! Debug version of licmgr not found.")
    set(LICMGR_LIBRARIES
        "${LICMGR_LIBRARY_RELEASE}"
    )
endif()

MARK_AS_ADVANCED(LICMGR_LIBRARIES)

message(STATUS "LICMGR_LIBRARIES=${LICMGR_LIBRARIES}")

# handle the QUIETLY and REQUIRED arguments and set
# LICMGR_FOUND to TRUE as appropriate
INCLUDE( FindPackageHandleStandardArgs )
FIND_PACKAGE_HANDLE_STANDARD_ARGS( LICMGR DEFAULT_MSG LICMGR_INCLUDE_DIR LICMGR_LIBRARIES)
