# This module defines

# QTSOLUTIONS_PROPERTYBROWSER_LIBRARY
# QTSOLUTIONS_SINGLEAPPLICATION_LIBRARY
# QTSOLUTIONS_FOUND, if false, do not try to link to osgEarth
# QTSOLUTIONS_INCLUDE_DIR, where to find the headers
# QTSOLUTIONS_LIBRARIES, to use in target_link_libraries()

# to use this module, set variables to point to the osgEarth install directory
# QTSOLUTIONSDIR or QTSOLUTIONS_DIR
FIND_PATH(QTSOLUTIONS_DIR version.txt
    $ENV{QTSOLUTIONS_DIR}
    $ENV{QTSOLUTIONSDIR}
    $ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/qt-solutions
    ${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/qt-solutions
    NO_DEFAULT_PATH
)
FIND_PATH(QTSOLUTIONS_INCLUDE_DIR QtSolutions
    PATH_SUFFIXES include
    PATHS
      ${QTSOLUTIONS_DIR}/v/${SYSTEM_ID}
      /sw # Fink
      /opt/local # DarwinPorts
      /opt/csw # Blastwave
      /opt
      /usr/freeware
      /usr/
      /usr/local
      C:/Program Files/osgEarth
)

FUNCTION(QTSOLUTIONS_FIND_LIBRARY module library)
    FIND_LIBRARY(${module} ${library}
    PATH_SUFFIXES
        lib
        lib64
    PATHS
        ${QTSOLUTIONS_DIR}/v/${SYSTEM_ID}
        /sw # Fink
        /opt/local # DarwinPorts
        /opt/csw # Blastwave
        /opt
        /usr/freeware
        /usr/
        /usr/local
        C:/Program Files/osgEarth
    )
    MARK_AS_ADVANCED(${module})
ENDFUNCTION(QTSOLUTIONS_FIND_LIBRARY module library)

QTSOLUTIONS_FIND_LIBRARY(QTSOLUTIONS_PROPERTYBROWSER_LIBRARY QtSolutions_PropertyBrowser-head)
QTSOLUTIONS_FIND_LIBRARY(QTSOLUTIONS_PROPERTYBROWSER_LIBRARY_DEBUG QtSolutions_PropertyBrowser-headd)

QTSOLUTIONS_FIND_LIBRARY(QTSOLUTIONS_SINGLEAPPLICATION_LIBRARY QtSolutions_SingleApplication-head)
QTSOLUTIONS_FIND_LIBRARY(QTSOLUTIONS_SINGLEAPPLICATION_LIBRARY_DEBUG QtSolutions_SingleApplication-headd)

if(QTSOLUTIONS_PROPERTYBROWSER_LIBRARY_DEBUG)
    set(QTSOLUTIONS_LIBRARIES
        optimized "${QTSOLUTIONS_PROPERTYBROWSER_LIBRARY}"
        debug     "${QTSOLUTIONS_PROPERTYBROWSER_LIBRARY_DEBUG}"
    )
else()
    message(STATUS "Attention! Debug version of property browser not found. Release version of property browser will *crash* in debug build.")
    set(QTSOLUTIONS_LIBRARIES
        "${QTSOLUTIONS_PROPERTYBROWSER_LIBRARY}"
    )
endif()
if(QTSOLUTIONS_SINGLEAPPLICATION_LIBRARY_DEBUG)
    set(QTSOLUTIONS_LIBRARIES ${QTSOLUTIONS_LIBRARIES}
        optimized "${QTSOLUTIONS_SINGLEAPPLICATION_LIBRARY}"
        debug     "${QTSOLUTIONS_SINGLEAPPLICATION_LIBRARY_DEBUG}"
    )
else()
    set(QTSOLUTIONS_LIBRARIES ${QTSOLUTIONS_LIBRARIES}
        "${QTSOLUTIONS_SINGLEAPPLICATION_LIBRARY}"
    )
endif()
message(STATUS "QTSOLUTIONS_LIBRARIES=${QTSOLUTIONS_LIBRARIES}")

INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set QUAZIP_FOUND to TRUE if
# all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(QTSOLUTIONS  DEFAULT_MSG  QTSOLUTIONS_LIBRARIES QTSOLUTIONS_INCLUDE_DIR)

MARK_AS_ADVANCED(QTSOLUTIONS_INCLUDE_DIR QTSOLUTIONS_LIBRARIES)
