# Author Ivan Charamisinau <icharamisinau@topcon.com>

FIND_PATH(DECAP_DIR version.txt
    "${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/DeCap"
    NO_DEFAULT_PATH
)
SET(DECAP_DIR  "${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/DeCap")

FIND_PATH(DECAP_INCLUDE_DIR DeCapAuthenticator.h
    ${DECAP_DIR}/3.0.1.5/${SYSTEM_ID}/include
    ${DECAP_DIR}/include
    $ENV{DECAP_DIR}/include
    $ENV{DECAP_DIR}
    NO_DEFAULT_PATH
)

FIND_PATH(DECAP_INCLUDE_DIR DeCapAuthenticator.h)

FUNCTION(FIND_DECAP_LIBRARY module library)
    FIND_LIBRARY(${module} ${library}
    PATH_SUFFIXES
        lib
        lib64
    PATHS
        ${DECAP_DIR}/3.0.1.5/${SYSTEM_ID}
        ${DECAP_DIR}
    )
    MARK_AS_ADVANCED(${module})
ENDFUNCTION(FIND_DECAP_LIBRARY module library)

FIND_DECAP_LIBRARY(DECAP_LIBRARY_RELEASE DeCapAuthenticator)

FIND_DECAP_LIBRARY(DECAP_LIBRARY_DEBUG DeCapAuthenticatord)

SET(DECAP_LIBRARIES_RELEASE
    ${DECAP_LIBRARY_RELEASE}
)
MARK_AS_ADVANCED(DECAP_LIBRARIES_RELEASE)

SET(DECAP_LIBRARIES_DEBUG
    ${DECAP_LIBRARY_DEBUG}
)
MARK_AS_ADVANCED(DECAP_LIBRARIES_DEBUG)

if(DECAP_LIBRARY_DEBUG)
    set(DECAP_LIBRARIES
        optimized "${DECAP_LIBRARY_RELEASE}"
        debug     "${DECAP_LIBRARY_DEBUG}"
    )
else()
    message(STATUS "Attention! Debug version of DECAP not found.")
    set(DECAP_LIBRARIES
        "${DECAP_LIBRARY_RELEASE}"
    )
endif()

message(STATUS "DECAP_LIBRARIES=${DECAP_LIBRARIES}")

# handle the QUIETLY and REQUIRED arguments and set
# DECAP_FOUND to TRUE as appropriate
INCLUDE( FindPackageHandleStandardArgs )
FIND_PACKAGE_HANDLE_STANDARD_ARGS( DECAP DEFAULT_MSG DECAP_INCLUDE_DIR DECAP_LIBRARIES)
