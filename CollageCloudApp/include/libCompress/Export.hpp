#ifndef _LIBCOMPRESS_EXPORT_HPP_
#define _LIBCOMPRESS_EXPORT_HPP_ 1

#if defined(_MSC_VER)
#pragma warning( disable : 4251 )
#endif

#if defined(_MSC_VER) || defined(__CYGWIN__) || defined(__MINGW32__) || defined( __BCPLUSPLUS__)  || defined( __MWERKS__)
#  if defined( LIBCOMPRESS_STATIC_LIB ) || defined( USE_STATIC )
#    define LIBCOMPRESS_EXPORT
#  elif defined( LIBCOMPRESS_LIBRARY )
#    define LIBCOMPRESS_EXPORT   __declspec(dllexport)
#  else
#    define LIBCOMPRESS_EXPORT   __declspec(dllimport)
#  endif /* LIBCOMPRESS_EXPORT */
#else
#  define LIBCOMPRESS_EXPORT
#endif  

// set up define for whether member templates
// are supported by VisualStudio compilers.
#ifdef _MSC_VER
# if (_MSC_VER >= 1300)
#  define __STL_MEMBER_TEMPLATES
# endif
#endif

/* Define NULL pointer value */

#ifndef NULL
#ifdef  __cplusplus
#define NULL    0
#else
#define NULL    ((void *)0)
#endif
#endif

#endif

