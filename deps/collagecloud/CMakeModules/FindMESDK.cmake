# Author Rafael Gaitan <rgaitan@topcon.com>

FIND_PATH(MESDK_DIR version.txt
    $ENV{MESDK_DIR}
    ${EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/mesdk
    $ENV{EXTERNALS_DEPS_DIR}/${DEPS_PLATFORM}/mesdk
    NO_DEFAULT_PATH
)

FIND_PATH(MESDK_INCLUDE_DIR topsdk/mesdk/Includes/mesdkAPI.h
    ${MESDK_DIR}/4.0/${SYSTEM_ID}/include
    ${MESDK_DIR}/include
    $ENV{MESDK_DIR}/include
    $ENV{MESDK_DIR}
    $ENV{MESDK_DIR}/include
    $ENV{MESDK_DIR}
    NO_DEFAULT_PATH
)

FIND_PATH(MESDK_INCLUDE_DIR mesdk/mesdkAPI.h)

FUNCTION(FIND_MESDK_LIBRARY module library)
    FIND_LIBRARY(${module} ${library}
    PATH_SUFFIXES
        lib
        lib64
    PATHS
        ${MESDK_DIR}/4.0/${SYSTEM_ID}
        ${MESDK_DIR}
    )
    MARK_AS_ADVANCED(${module})
ENDFUNCTION(FIND_MESDK_LIBRARY module library)

FIND_MESDK_LIBRARY(MESDK_LIBRARY_RELEASE mesdklib)

FIND_MESDK_LIBRARY(MESDK_LIBRARY_DEBUG mesdklibd)

SET(MESDK_LIBRARIES_RELEASE
    ${MESDK_LIBRARY_RELEASE}
)
MARK_AS_ADVANCED(MESDK_LIBRARIES_RELEASE)

SET(MESDK_LIBRARIES_DEBUG
    ${MESDK_LIBRARY_DEBUG}
)
MARK_AS_ADVANCED(MESDK_LIBRARIES_DEBUG)

if(MESDK_LIBRARY_DEBUG)
    set(MESDK_LIBRARIES
        optimized "${MESDK_LIBRARY_RELEASE}"
        debug     "${MESDK_LIBRARY_DEBUG}"
    )
else()
    message(STATUS "Attention! Debug version of mesdk not found.")
    set(MESDK_LIBRARIES
        "${MESDK_LIBRARY_RELEASE}"
    )
endif()

message(STATUS "MESDK_LIBRARIES=${MESDK_LIBRARIES}")

# handle the QUIETLY and REQUIRED arguments and set
# MESDK_FOUND to TRUE as appropriate
INCLUDE( FindPackageHandleStandardArgs )
FIND_PACKAGE_HANDLE_STANDARD_ARGS( MESDK DEFAULT_MSG MESDK_INCLUDE_DIR MESDK_LIBRARIES)
